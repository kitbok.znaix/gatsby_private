import React from "react";
import NavBar from "./NavBar";
import "../styles/global.css";

export default function Layout({ children }) {
  return (
    <div className="layout">
      <NavBar />
      <div className="content">
        {/* Content for Each Page children from pages/index.js <Layout>all here are children</Layout> */}
        {children}
      </div>
      <footer>
        <p>&copy; Copyright 2023 KiT</p>
      </footer>
    </div>
  );
}
