import React from "react";
import Layout from "../components/Layout";
function NotFound() {
  return (
    <Layout>
      <div>
        <h2>404</h2>
        <p>Sorry, Page doesn't exist</p>
      </div>
    </Layout>
  );
}

export default NotFound;
